//! Listen and react to touch events.
pub use iced_native::touch::{Event, Finger};
